"""
Copyright (C) 2018
     Andrew Hayzen <ahayzen@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from datetime import date, timedelta
from requests.exceptions import HTTPError

from . import BaseModel


class DownloadModel(BaseModel):
    NAME = "Download Model"

    def __init__(self, api, extra={}):
        BaseModel.__init__(self, api, extra)

        self.downloads = []
        self.skip_today = extra.get("skip_today", False)
        self.start_date = extra.get("start_date")

    def __len__(self):
        return len(self.downloads)

    def load_generator(self):
        this_date = date.fromisoformat(self.start_date)
        today = date.today()
        i = 0

        while True:
            try:
                downloads = self.api.get_downloads_for_date(this_date.year, this_date.month, this_date.day)
            except HTTPError:
                downloads = {
                    "countries": {},
                    "downloads": 0,
                    "delta_downloads": 0,
                    "flatpak_versions": {},
                    "ostree_versions": {},
                    "refs": {},
                    "updates": 0
                }

            self.downloads.append([this_date, downloads])

            this_date += timedelta(days=1)

            if (self.skip_today and this_date >= today) or (not self.skip_today and this_date > today):
                break

            yield i
            i += 1

    def downloads_by_date(self):
        return self.downloads
