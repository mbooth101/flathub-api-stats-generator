"""
Copyright (C) 2018, 2019
     Andrew Hayzen <ahayzen@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from os import path

from api import BaseAPI


class GithubFlathubAPI(BaseAPI):
    APP_ID_BLACKLIST = [
        "ansible-playbook", "buildbot", "BaseApp", "flathub",
        "io.github.Hexchat.Plugin.", "linux-store-", "org.flatpak.Builder",
        "org.freedesktop.Platform", "org.freedesktop.Sdk", "org.gtk.Gtk3theme",
        "org.kde.KStyle", "org.kde.PlatformTheme", "sample-app",
        "shared-modules",
    ]
    BASE_URL = "https://api.github.com/"
    DATETIME_FORMAT = "%Y-%m-%dT%H:%M:%SZ"
    FIELD_CREATION_DATE = "created_at"
    FIELD_ID = "name"
    TOKEN = ""
    TOKEN_FILE = ".github-token"
    NAME = "Flathub"

    @classmethod
    def check(cls, extra):
        if "token" in extra and extra["token"] is not None:
            cls.TOKEN = extra["token"]
        else:
            if not path.exists(cls.TOKEN_FILE):
                with open(cls.TOKEN_FILE, "w") as f:
                    print("Go to https://github.com/settings/tokens generate "
                          "a token, no permissions are required.")
                    print("Github only allows 50 queries a day without a "
                          "token.")
                    print("If you think you'll use less, then leave this "
                          "blank.")
                    token = input("Please enter your token: ").strip()

                    f.write("%s\n" % token)
                    cls.TOKEN = token
            else:
                with open(cls.TOKEN_FILE, "r") as f:
                    cls.TOKEN = f.read().strip()

    @classmethod
    def get_apps(cls):
        i = 1

        while True:  # loop until no records are returned
            record = cls.get_json_data(
                cls.BASE_URL,
                path.join("orgs", "flathub",
                          "repos?type=all&page=%i&per_page=100" % i),
                auth=("user", cls.TOKEN),
            )

            if len(record) == 0:
                break

            for data in record:
                # Check that app is not in the blacklist
                if cls.is_valid_app_id(data[cls.FIELD_ID]):
                    yield data

            i += 1

    @classmethod
    def get_app_by_id(cls, app_id):
        return cls.get_json_data(
            cls.BASE_URL,
            path.join("repos", "flathub", app_id),
            auth=("user", cls.TOKEN),
        )

    @classmethod
    def get_app_by_record(cls, record):
        # GitHub has enough info in the record from get_apps
        # so here we can just return the record back :-)
        return record

    @classmethod
    def is_valid_app_id(cls, app_id):
        # Check the app_id doesn't contain any parts of the blacklist
        return sum(1 for b in cls.APP_ID_BLACKLIST if b in app_id) == 0
