"""
Copyright (C) 2018, 2019
     Andrew Hayzen <ahayzen@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

# We want to be headless
import matplotlib
matplotlib.use("Agg")

import matplotlib.pyplot as plt
from cycler import cycler

import numpy as np
from matplotlib import cm

# Small colour map for the graphs, this assumes a max of 9 sets of data
# Fixed values from Set1 from future matplotlib
# https://matplotlib.org/examples/color/colormaps_reference.html
small_cycler = cycler("color", [
  "#e51217", "#337fb9", "#4bb149", "#9a4da4",
  "#ff8003", "#ffff30", "#a85524", "#f882c0",
  "#9a9a9a",
])
# Large colour map for the graphs, this assumes a max of 40 sets of data
# Fixed values from tab20b and tab20c from future matplotlib
# https://matplotlib.org/examples/color/colormaps_reference.html
large_cycler = cycler("color", [
  # tab20b colours
  "#36387a", "#5153a4", "#6b6ed0", "#9d9fdf",
  "#637a36", "#8da351", "#b6d06b", "#cfdc9e",
  "#8d6d2d", "#bf9f36", "#e8bb51", "#e8cc95",
  "#853936", "#af4749", "#d7616b", "#e8979e",
  "#7c3f74", "#a75095", "#cf6dbe", "#df9fd7",
  # tab20c colours
  "#2c83be", "#6bb0d7", "#9fcbe2", "#c7dcef",
  "#e75407", "#fd8e3a", "#fdaf6c", "#fdd1a4",
  "#2ca553", "#74c577", "#a2da9c", "#c8eac1",
  "#766bb2", "#9f9bc9", "#bdbedd", "#dbdbec",
  "#636363", "#979797", "#bebebe", "#dadada",
])


class Graph:
    CHART_LINE = 0
    CHART_PERCENTAGE_STACKED_BAR = 1

    @staticmethod
    def from_report(filename, report, chart=CHART_LINE):
        if report.HAS_DATA_ZIPPED:
            Graph.new()

            kwargs = report.data_zipped_kwargs()

            # Pick cycler depending on amount of data
            if len(report.data_zipped()) > 9:
                plt.rcParams["axes.prop_cycle"] = large_cycler
            else:
                plt.rcParams["axes.prop_cycle"] = small_cycler

            if chart == Graph.CHART_LINE:
                if len(kwargs) > 0 and ("x_date" in kwargs[0] or "y_date" in kwargs[0]):
                    method = Graph.set_data_with_dates
                else:
                    method = Graph.set_data

                for extra, data_series in zip(kwargs, report.data_zipped()):
                    method(*data_series, **extra)

                if len(kwargs) > 0 and "label" in kwargs[0]:
                    Graph.show_legend(len(kwargs))
            elif chart == Graph.CHART_PERCENTAGE_STACKED_BAR:
                totals = list(sum(data) or 1 for data in zip(*[list(series)[1] for series in report.data_zipped()]))
                x_series = []
                y_series = []

                for extra, data_series in zip(kwargs, report.data_zipped()):
                    x, y = data_series

                    x_series.append(x)
                    y_series.append(list(value / totals[i] for i, value in enumerate(y)))

                Graph.set_data_stack_bar(x_series, y_series, x_date=kwargs[0].get("x_date", False),
                                         y_date=kwargs[0].get("y_date", False), extras=kwargs)
            else:
                print("Unknown chart type: %s" % chart)
                return

            Graph.set_axis(*report.axis)
            Graph.set_title(report.title)

            Graph.save(filename)

            print("Generated '%s'" % filename)
        else:
            print("Report (%s) does not support graph output" % report)

    @staticmethod
    def new():
        plt.clf()
        plt.figure(figsize=(10, 10), dpi=100)

    @staticmethod
    def save(filename):
        # We don't want margins for now, so set to 0.0
        plt.margins(x=0.0, y=0.0, tight=False)

        # Ensure we don't show negative in the y-axis
        plt.ylim(bottom=0)

        plt.savefig(filename, bbox_inches="tight")

    @staticmethod
    def set_axis(x_label, y_label):
        plt.xlabel(x_label)
        plt.ylabel(y_label)

    @staticmethod
    def set_data(x_series, y_series, **extra):
        plt.plot(x_series, y_series, **extra)

    @staticmethod
    def set_data_stack_bar(x_series, y_series, x_date=False, y_date=False, extras=[]):
        if x_date:
            plt.xticks(rotation=30)

        if y_date:
            plt.yticks(rotation=30)

        bottom = [0] * len(x_series[0])

        colors = iter(plt.rcParams['axes.prop_cycle'])
        artists = []

        for i, (x, y, extra) in enumerate(zip(x_series, y_series, extras)):
            try:
                color = next(colors)["color"]
            except StopIteration:
                # If we have reached then end, then restart colour cycle
                colors = iter(plt.rcParams['axes.prop_cycle'])
                color = next(colors)["color"]

            artists.append(plt.bar(x, y, bottom=bottom, width=1.01, color=color, edgecolor=color,
                                   label=extra.get("label", None)))

            bottom = list(map(sum, zip(bottom, y)))

        if len(extras) > 0 and "label" in extras[0]:
            Graph.show_legend(len(extras), artists, [extra["label"] for extra in extras])

        if x_date or y_date:
            plt.gcf().autofmt_xdate()

        plt.ylim(ymax=1)

    @staticmethod
    def set_data_with_dates(x_series, y_series, x_date=False, y_date=False,
                            **extra):
        if x_date:
            plt.xticks(rotation=30)

        if y_date:
            plt.yticks(rotation=30)

        plt.plot_date(x_series, y_series, "-", xdate=x_date, ydate=y_date,
                      drawstyle='steps-mid', **extra)

    @staticmethod
    def set_title(title):
        plt.title(title)

    @staticmethod
    def show_legend(count, artists=[], labels=[]):
        if len(artists) > 0 and len(labels) > 0:
            # loc is upper left
            plt.legend(list(reversed(artists)), list(reversed(labels)), loc=2,
                       bbox_to_anchor=(1.01, 1), fontsize=8)
        else:
            plt.legend(loc=2, bbox_to_anchor=(1.01, 1), fontsize=8)  # loc is upper left
