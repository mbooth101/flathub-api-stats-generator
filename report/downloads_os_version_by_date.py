"""
Copyright (C) 2018
     Andrew Hayzen <ahayzen@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from datetime import datetime

from . import BaseReport
from model import DownloadModel


class DownloadsOSTreeVersionByDateReport(BaseReport):
    HAS_DATA_STRING = False  # TODO: string output
    HAS_DATA_ZIPPED = True
    MODEL = DownloadModel

    def __init__(self, model, args=[]):
        BaseReport.__init__(self, model)

        for arg in args:
            try:
                key, value = arg
            except ValueError:
                print("Expected exactly one equals, got these parts: %s" % arg)
                break

            if key == "app-id":
                print("downloads_by_ostree_version does not support app-id filtering")
            else:
                print("Unknown report-arg %s=%s" % (key, value))

        # Pre read all the versions
        self.versions = []

        for _, data in self.model.downloads_by_date():
            for version in data["ostree_versions"]:
                if version not in self.versions:
                    self.versions.append(version)

        # Sort reverse so new appears from the bottom
        self.versions.sort(reverse=True)

    @property
    def axis(self):
        return ["Time", "Downloads"]

    def data_zipped(self):
        return [
            zip(
                *(
                    [date, data["ostree_versions"].get(version, 0)]
                    for date, data in self.model.downloads_by_date()
                )
            ) for version in self.versions
        ]

    def data_zipped_kwargs(self):
        return [
            {
                "label": version,
                "x_date": True,
            } for version in self.versions
        ]

    @property
    def title(self):
        return (
            "Downloads by ostree version in %s over time - %s" % (
                self.model.api.NAME,
                datetime.strftime(datetime.utcnow(), "%Y-%m-%d %H:%M:%S")
            )
        )
