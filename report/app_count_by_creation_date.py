"""
Copyright (C) 2018
     Andrew Hayzen <ahayzen@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from datetime import datetime

from . import BaseReport
from model import AppModel


class AppCountByCreationDateReport(BaseReport):
    HAS_DATA_STRING = True
    HAS_DATA_ZIPPED = True
    MODEL = AppModel

    def __init__(self, model, args=[]):
        BaseReport.__init__(self, model)

        for arg in args:
            try:
                key, value = arg
            except ValueError:
                print("Expected exactly one equals, got these parts: %s" % arg)
                break

            if key == "app-id":
                print("count_by_time does not support app-id filtering")
            else:
                print("Unknown report-arg %s=%s" % (key, value))

    @property
    def axis(self):
        return ["Time", "Apps"]

    def data_string(self):
        return (
            "%s\t%i" % (date.isoformat(timespec="minutes"), count)
            for date, count in zip(*self.data_zipped())
        )

    def data_zipped(self):
        return [
            zip(
                *(
                    [app["creationDate"], i + 1]
                    for i, app in enumerate(
                        self.model.apps_ordered_by("creationDate"))
                )
            )
        ]

    def data_zipped_kwargs(self):
        return [
            {
                "x_date": True,
            }
        ]

    @property
    def title(self):
        return (
            "Apps in %s over time - %s" % (
                self.model.api.NAME,
                datetime.strftime(datetime.utcnow(), "%Y-%m-%d %H:%M:%S")
            )
        )
