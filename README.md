This is a basic tool for generating data and graphs about apps within [Flathub](http://flathub.org/).

# Common Usages

## Show me the number of apps over time

```
python3 main.py --report count_by_time --type graph --output out.png
```

## Show me the downloads for a specific app-id over time

```
python3 main.py --report downloads_by_app --type graph --output out.png --report-args app-id=org.gnome.Recipes
```

## Show me the downloads for multiple app-id's over time

```
python3 main.py --report downloads_by_app --type graph --output out.png --report-args app-id=org.gnome.Recipes,app-id=org.gnome.gedit
```

## Output app names with their first creation date

```
python3 main.py --api flathub --report list_by_date --output output.dat --type data
```

## Show me a stacked bar chart of the percentage of flatpak versions

```
python3 main.py --report downloads_by_flatpak_version --type graph:stacked_bar --output out.png
```

## Show me the number of downloads and known updates per app-id in a data file

```
python3 main.py --report downloads_by_app --type data --output out.dat
```

# General Usage

The python script can be used with arguments. There are options for which API, output filenames, API token, report modes, data output type (graph or space delimited file).

Use `python3 main.py --help` to find out how to use these.

# Rational

I have been running a cronjob hourly on a local machine which runs `flatpak remote-ls --app flathub | wc -l`, checks if the count has changed from the previous hour, writes to a data file and then finally generates a PNG graph. This PNG is then a nice graph showing the growth of flatpak applications in flathub over time.

This works well, however it requires my local machine to be running to update the statistics. Now I could just install `flatpak` onto a server, however it pulls in a lot of dependencies that aren't relevant for a server environment.

Therefore I decided to create a simple tool which queries available APIs to find out the same information (and kept the code vaguely flexibile to doing further reports :-) ).

This then has the advantage of being able to run on a server without many dependencies (just python3, python3-matplotlib and their dependencies) and it allows for generating the graph back to zero - before I was missing data.

# Example

Below is the example output of apps in Flathub over time, daily generated graphs can be found at [https://ahayzen.com/direct/flathub.html](https://ahayzen.com/direct/flathub.html)

![Flathub Stats](http://ahayzen.com/direct/flathub.png)

# APIs

  * https://flathub.org/api/v1/
  * https://api.github.com/

Flathub is the default API which is used, but GitHub can also be used with personal tokens (this is useful for comparing any apps that are missing from the store).

The GitHub API should now be considered legacy, it was used mainly before the Flathub API existed.

## Tokens

When using the GitHub API a guest user is limited to 50 requests per day, one can [generate a token](https://github.com/settings/tokens) which allows many more :-)

This is then passed to the python script either with `---token` or by placing the token inside a file named `.github-token` in the run directory.

