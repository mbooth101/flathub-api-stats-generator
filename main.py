#!/usr/bin/python3
"""
Copyright (C) 2018, 2019
     Andrew Hayzen <ahayzen@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import argparse
from os import makedirs, path

from dat import DatWriter
from graph import Graph
from report import (
    AppCountByCreationDateReport, AppsByCreationDateReport, AppsByNameReport,
    BaseReport, DownloadsByDateReport, DownloadsFlatpakMajorVersionByDateReport, DownloadsFlatpakVersionByDateReport,
    DownloadsOSTreeVersionByDateReport, DownloadsAppByDateReport, DownloadsArchByDateReport,
    DownloadsOnlyAppsByDateReport, DownloadsOnlyNvidiaRuntimesByDateReport,
    DownloadsOnlyRuntimeByDateReport
)


class StdProgress:
    def __init__(self, initial_text):
        self.previous_longest_line = len(initial_text)
        print(initial_text, end="", flush=True)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        print("\r".ljust(self.previous_longest_line), end="\r", flush=True)

    def progress(self, text):
        print(("\r%s" % text).ljust(self.previous_longest_line),
              end="", flush=True)
        self.previous_longest_line = max(self.previous_longest_line,
                                         len(text) + 1)


def class_for_report(mode):
    report = BaseReport

    if mode == "count_by_time":
        report = AppCountByCreationDateReport
    elif mode == "list_by_date":
        report = AppsByCreationDateReport
    elif mode == "list_by_name":
        report = AppsByNameReport
    elif mode == "downloads_by_app":
        report = DownloadsAppByDateReport
    elif mode == "downloads_by_arch":
        report = DownloadsArchByDateReport
    elif mode == "downloads":
        report = DownloadsByDateReport
    elif mode == "downloads_by_flatpak_major_version":
        report = DownloadsFlatpakMajorVersionByDateReport
    elif mode == "downloads_by_flatpak_version":
        report = DownloadsFlatpakVersionByDateReport
    elif mode == "downloads_by_ostree_version":
        report = DownloadsOSTreeVersionByDateReport
    elif mode == "downloads_only_apps":
        report = DownloadsOnlyAppsByDateReport
    elif mode == "downloads_only_nvidia_runtimes":
        report = DownloadsOnlyNvidiaRuntimesByDateReport
    elif mode == "downloads_only_runtimes":
        report = DownloadsOnlyRuntimeByDateReport
    else:
        print("Unknown mode: %s" % mode)

    return report


def arguments_program(api, extra, reports, output, types):
    reports = list(map(class_for_report, reports))
    models = {}

    if len(types) == 1 and len(reports) > 1:
        types = types * len(reports)
    elif len(types) != len(reports):
        print("Either one type should be given or the amount should match the number of reports")
        return

    if len(reports) != len(output):
        print("Number of reports and outputs does not match")
        return

    for report in reports:
        if report.MODEL.NAME not in models:
            models[report.MODEL.NAME] = report.MODEL(api=api, extra=extra)

    for model in models.values():
        with StdProgress("Preparing %s, please wait..." % model.NAME) as std:
            for i in model.load_generator():
                std.progress("Loading %s: %i, please wait..." % (model.NAME, i))

    report_args = []
    if extra["report_args"] is not None:
        report_args = [option.split("=") for option in extra["report_args"].split(",")]

    for i, report in enumerate(reports):
        report = report(models[report.MODEL.NAME], report_args)

        if types[i] == "data":
            DatWriter.from_report(output[i], report)
        elif types[i] == "graph":
            Graph.from_report(output[i], report)
        elif types[i] == "graph:line":
            Graph.from_report(output[i], report, chart=Graph.CHART_LINE)
        elif types[i] == "graph:stacked_bar":
            Graph.from_report(output[i], report, chart=Graph.CHART_PERCENTAGE_STACKED_BAR)
        else:
            print("Unknown type: %s" % types[i])


if __name__ == "__main__":
    from api import FlathubAPIv1, GithubFlathubAPI

    parser = argparse.ArgumentParser(description="Flathub API stats generator")
    parser.add_argument("--api", choices=["flathub", "github"],
                        default="flathub", help="Which API to query")
    parser.add_argument("--token", default=None,
                        help="Token to use, eg GitHub token")
    parser.add_argument("-o", "--output", help="Output filename, eg 'out.png'", nargs="*")
    parser.add_argument("--report", choices=["count_by_time", "list_by_date", "list_by_name",
                                             "downloads", "downloads_by_app", "downloads_by_arch",
                                             "downloads_by_flatpak_major_version", "downloads_by_flatpak_version",
                                             "downloads_by_ostree_version", "downloads_only_apps",
                                             "downloads_only_nvidia_runtimes", "downloads_only_runtimes"],
                        default="count_by_time",
                        help="Which mode is used to generate data", nargs="*")
    parser.add_argument("--type", choices=["data", "graph", "graph:line", "graph:stacked_bar"], default="graph",
                        help="Which type of data should be created, a graph "
                        "image or space delimited data file", nargs="*")
    parser.add_argument("--report-args", default=None,
                        help="Extra args for the report split be comma, eg a=b,c=d or app-id=org.gnome.Recipes")
    parser.add_argument("--model-skip-today", action="store_true",
                        help="If the DownloadsModel should skip statistics for today")
    parser.add_argument("--cache-dir", default=None, help="Location to use as a cache directory for models")
    parser.add_argument("--start-date", default="2018-04-29",
                        help="The date from which to start fetching models, e.g.: 2018-04-29")

    args = parser.parse_args()

    if args.api == "github":
        api = GithubFlathubAPI
    elif args.api == "flathub":
        api = FlathubAPIv1
    else:
        print("Unknown API: %s" % args.api)
        exit()

    if args.cache_dir is not None:
        if not path.exists(args.cache_dir):
            makedirs(args.cache_dir, exist_ok=True)

        # Ensure that the existing or created path is a dir
        if not path.isdir(args.cache_dir):
            print("Cache directory is not a directory: %s" % args.cache_dir)
            exit()

        api.CACHE_DIR = args.cache_dir

    if args.output:
        try:
            arguments_program(api=api, extra={"token": args.token, "report_args": args.report_args,
                                              "skip_today": args.model_skip_today, "start_date": args.start_date},
                              reports=args.report, output=args.output,
                              types=args.type)
        except KeyboardInterrupt:
            print("KeyboardInterrupt: Exiting")
    else:
        print("Unknown mode, either specify an output or interactive mode.")

        parser.print_help()
